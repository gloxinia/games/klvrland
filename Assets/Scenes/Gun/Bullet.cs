using UnityEngine;

namespace Klvrland.Gun
{
    public class Bullet : MonoBehaviour
    {
        /// <summary>
        /// Moving speed (m/s)
        /// </summary>
        public float Speed { get; set; } = 0.0f;

        private float LifeTimer = 0.0f;

        private readonly float LifeTime = 1.0f;

        void Update()
        {
            LifeTimer += Time.deltaTime;
            transform.Translate(Vector3.forward * Time.deltaTime * Speed);

            if (LifeTime < LifeTimer)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
