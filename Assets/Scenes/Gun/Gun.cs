using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Klvrland.Gun
{
    public class Gun : MonoBehaviour
    {
        public GameObject BulletPrefab;

        private bool IsCool = false;
        private float CoolTimer = 0.0f;
        private XRBaseInteractable Interactable;

        private readonly float CoolTime = 0.2f;
        private readonly float BulletSpeed = 50.0f;

        void Awake()
        {
            Interactable = GetComponent<XRBaseInteractable>();
        }

        private void OnEnable()
        {
            Interactable.activated.AddListener(Shoot);
        }

        private void OnDisable()
        {
            Interactable.activated.RemoveListener(Shoot);
        }

        void Update()
        {
            CoolTimer += Time.deltaTime;

            if (IsCool == false && CoolTime < CoolTimer)
            {
                IsCool = true;
            }
        }

        private void Shoot(BaseInteractionEventArgs args)
        {
            if (IsCool)
            {
                var ShootedBullet = Instantiate(BulletPrefab, transform.position, transform.rotation);
                ShootedBullet.GetComponent<Bullet>().Speed = BulletSpeed;

                IsCool = false;
                CoolTimer = 0.0f;
            }
        }
    }
}
