using UnityEngine;

namespace Klvrland.Gun
{
    public class Target : MonoBehaviour
    {
        private bool IsDead = false;
        private float RevivalTimer = 0.0f;
        private float Rotation = 0.0f;

        private readonly float RevivalTime = 1.0f;
        private readonly float DefaultRotation = 0.0f;
        private readonly float FinalRotation = 75.0f;
        private readonly float DownRotateSpeed = 360.0f;
        private readonly float UpRotateSpeed = 45.0f;

        void Update()
        {
            if (IsDead)
            {
                // 死んでいる場合
                if (Rotation < FinalRotation)
                {
                    // 最後まで倒れていない場合 倒れる
                    Rotation += DownRotateSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(DownRotateSpeed, 0.0f, 0.0f) * Time.deltaTime);
                }
                else
                {
                    // 最後まで倒れた場合 復活時間を計測する
                    RevivalTimer += Time.deltaTime;
                }

                if (RevivalTime < RevivalTimer)
                {
                    // 復活時間を過ぎた場合 生き返る
                    // このブロックを抜け次フレームから通過しない
                    IsDead = false;
                }
            }
            else
            {
                // 生きている場合
                if (Rotation > DefaultRotation)
                {
                    // 少しでも倒れている場合 起き上がる
                    Rotation -= UpRotateSpeed * Time.deltaTime;
                    transform.Rotate(new Vector3(-UpRotateSpeed, 0.0f, 0.0f) * Time.deltaTime);
                }
            }
        }

        public void Hit()
        {
            if (IsDead == false)
            {
                IsDead = true;
                RevivalTimer = 0.0f;
            }
        }
    }
}
