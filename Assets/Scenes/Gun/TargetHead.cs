using UnityEngine;

namespace Klvrland.Gun
{
    public class TargetHead : MonoBehaviour
    {
        public GameObject Target;

        private void OnTriggerEnter(Collider other)
        {
            Target.GetComponent<Target>().Hit();
            Debug.Log("HIT!!");
        }
    }
}
