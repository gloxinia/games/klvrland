using UnityEngine;
using UnityEngine.UI;

namespace Klvrland.ControllerWatcher
{
    public class TextUpdater : MonoBehaviour
    {
        public XRControllerWatcher Watcher;
        public Text TextLeft;
        public Text TextRight;

        void Update()
        {
            TextLeft.text = $"{Watcher.Left.DeviceName}" +
                $"\r\nTrigger: {Watcher.Left.TriggerButton}, {Watcher.Left.Trigger:F2}" +
                $"\r\nGrip: {Watcher.Left.GripButton}, {Watcher.Left.Grip:F2}" +
                $"\r\nPrimary: {Watcher.Left.PrimaryButton}, {Watcher.Left.PrimaryTouch}" +
                $"\r\nSecondary: {Watcher.Left.SecondaryButton}, {Watcher.Left.SecondaryTouch}" +
                $"\r\n2DAxis: {Watcher.Left.Primary2DAxis}, {Watcher.Left.Primary2DAxisClick}, {Watcher.Left.Primary2DAxisTouch}";

            TextRight.text = $"{Watcher.Right.DeviceName}" +
                $"\r\nTrigger: {Watcher.Right.TriggerButton}, {Watcher.Right.Trigger:F2}" +
                $"\r\nGrip: {Watcher.Right.GripButton}, {Watcher.Right.Grip:F2}" +
                $"\r\nPrimary: {Watcher.Right.PrimaryButton}, {Watcher.Right.PrimaryTouch}" +
                $"\r\nSecondary: {Watcher.Right.SecondaryButton}, {Watcher.Right.SecondaryTouch}" +
                $"\r\n2DAxis: {Watcher.Right.Primary2DAxis}, {Watcher.Right.Primary2DAxisClick}, {Watcher.Right.Primary2DAxisTouch}";
        }
    }
}
