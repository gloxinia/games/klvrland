using UnityEngine;

namespace Klvrland.Balloon.Mark1
{
    public class String : MonoBehaviour
    {
        public GameObject[] Vertexes = new GameObject[6];
        private LineRenderer LineRenderer;

        void Start()
        {
            LineRenderer = GetComponent<LineRenderer>();
            LineRenderer.positionCount = Vertexes.Length;
        }

        void Update()
        {
            for (int i = 0; i < Vertexes.Length; i++)
            {
                LineRenderer.SetPosition(i, Vertexes[i].transform.position);
            }
        }
    }
}
