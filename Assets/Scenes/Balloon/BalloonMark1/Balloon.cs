using UnityEngine;

namespace Klvrland.Balloon.Mark1
{
    public class Balloon : MonoBehaviour
    {
        private Rigidbody Rigidbody;
        private float ForceStlength = 0.1f;

        void Start()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            Rigidbody.AddForce(Vector3.up * ForceStlength);
        }
    }
}
