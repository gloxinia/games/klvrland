using UnityEngine;

namespace Klvrland.Balloon.Mark2
{
    public class Balloon : MonoBehaviour
    {
        private Rigidbody Rigidbody;
        private float ForceStlength = 0.05f;

        void Start()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            Rigidbody.AddForce(Vector3.up * ForceStlength);
        }
    }
}
