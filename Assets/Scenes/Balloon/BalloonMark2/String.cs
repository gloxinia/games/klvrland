using UnityEngine;

namespace Klvrland.Balloon.Mark2
{
    public class String : MonoBehaviour
    {
        private LineRenderer LineRenderer;

        void Start()
        {
            LineRenderer = GetComponent<LineRenderer>();
            LineRenderer.positionCount = transform.childCount;
        }

        void Update()
        {
            for (int i = 0; i < LineRenderer.positionCount; i++)
            {
                LineRenderer.SetPosition(i, transform.GetChild(i).transform.position);
            }
        }
    }
}
