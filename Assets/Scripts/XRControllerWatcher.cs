using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Klvrland
{
    public struct XRControllerInput
    {
        public InputDevice Device;
        public string DeviceName;
        public bool TriggerButton;
        public float Trigger;
        public bool GripButton;
        public float Grip;
        public bool PrimaryButton;
        public bool PrimaryTouch;
        public bool SecondaryButton;
        public bool SecondaryTouch;
        public Vector2 Primary2DAxis;
        public bool Primary2DAxisClick;
        public bool Primary2DAxisTouch;
    }

    public class XRControllerWatcher : MonoBehaviour
    {
        public XRControllerInput Left => LeftInput;
        public XRControllerInput Right => RightInput;

        private XRControllerInput LeftInput;
        private XRControllerInput RightInput;
        private InputDevice LeftController;
        private InputDevice RightController;

        private static readonly InputDeviceCharacteristics LeftControllerCharacteristics = InputDeviceCharacteristics.HeldInHand | InputDeviceCharacteristics.Controller | InputDeviceCharacteristics.Left;
        private static readonly InputDeviceCharacteristics RightControllerCharacteristics = InputDeviceCharacteristics.HeldInHand | InputDeviceCharacteristics.Controller | InputDeviceCharacteristics.Right;

        private void Start()
        {
            try
            {
                // TODO: Retry
                GetControllers();
            }
            catch (UnityException exception)
            {
                Debug.Log(exception.Message);
            }
        }

        private void Update()
        {
            UpdateInputValue(LeftController, ref LeftInput);
            UpdateInputValue(RightController, ref RightInput);
        }

        private void GetControllers()
        {
            LeftController = GetDevice(LeftControllerCharacteristics);
            LeftInput.DeviceName = LeftController.name;
            RightController = GetDevice(RightControllerCharacteristics);
            RightInput.DeviceName = RightController.name;
        }

        private InputDevice GetDevice(InputDeviceCharacteristics chara)
        {
            var Devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(chara, Devices);

            if (Devices.Count > 0)
            {
                return Devices[0];
            }
            else
            {
                throw new UnityException("Device was not found!!");
            }
        }

        private void UpdateInputValue(InputDevice device, ref XRControllerInput input)
        {
            // TriggerButton
            device.TryGetFeatureValue(CommonUsages.triggerButton, out input.TriggerButton);
            device.TryGetFeatureValue(CommonUsages.trigger, out input.Trigger);

            // GripButton
            device.TryGetFeatureValue(CommonUsages.gripButton, out input.GripButton);
            device.TryGetFeatureValue(CommonUsages.grip, out input.Grip);

            // PrimaryButton
            device.TryGetFeatureValue(CommonUsages.primaryButton, out input.PrimaryButton);
            device.TryGetFeatureValue(CommonUsages.primaryTouch, out input.PrimaryTouch);

            // SecondaryButton
            device.TryGetFeatureValue(CommonUsages.secondaryButton, out input.SecondaryButton);
            device.TryGetFeatureValue(CommonUsages.secondaryTouch, out input.SecondaryTouch);

            // Primary2DAxis
            device.TryGetFeatureValue(CommonUsages.primary2DAxis, out input.Primary2DAxis);
            device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out input.Primary2DAxisClick);
            device.TryGetFeatureValue(CommonUsages.primary2DAxisTouch, out input.Primary2DAxisTouch);
        }
    }
}
