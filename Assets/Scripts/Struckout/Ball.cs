using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Klvrland.Struckout
{
    public class Ball : MonoBehaviour
    {
        public GameObject HitEffectPrefab;

        private XRBaseInteractable Interactable;
        private Rigidbody Rigidbody;
        private Queue<Vector3> PositionQueue = new Queue<Vector3>();
        private Vector3 ReleasePosition;
        private float LifeTimer = 0.0f;
        private bool IsHit = false;

        private readonly float LifeTime = 10.0f;
        private readonly int BetweenFrame = 5;
        private readonly float FlyForceRatio = 1.5f;

        private void Awake()
        {
            Interactable = GetComponent<XRBaseInteractable>();
            Interactable.selectExited.AddListener(OnExited);

            Rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            if (Interactable.isSelected)
            {
                PositionQueue.Enqueue(transform.position);

                if (PositionQueue.Count > BetweenFrame)
                {
                    PositionQueue.Dequeue();
                }
            }

            if (IsHit)
            {
                LifeTimer += Time.deltaTime;
            }

            if (LifeTime < LifeTimer)
            {
                Destroy(this.gameObject);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag(Tag.Enemy.ToString()))
            {
                Instantiate(HitEffectPrefab, transform.position, transform.rotation);
                IsHit = true;
            }
        }

        private void OnExited(SelectExitEventArgs args)
        {
            ReleasePosition = transform.position;
            Fly();
        }

        private void Fly()
        {
            Vector3 FlyVector = ReleasePosition - PositionQueue.Dequeue();
            Rigidbody.AddForce(FlyVector * FlyForceRatio, ForceMode.Impulse);
        }
    }
}
