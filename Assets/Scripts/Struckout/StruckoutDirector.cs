using UnityEngine;

namespace Klvrland.Struckout
{
    public class StruckoutDirector : MonoBehaviour
    {
        private int Score = 0;

        public void Hit()
        {
            Score += 1;
        }
    }
}
