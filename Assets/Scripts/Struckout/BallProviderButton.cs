using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Klvrland.Struckout
{
    public class BallProviderButton : MonoBehaviour
    {
        public BallProvider BallProvider;

        private XRSimpleInteractable Interactable;

        private void Awake()
        {
            Interactable = GetComponent<XRSimpleInteractable>();
            Interactable.selectEntered.AddListener(OnPressed);
        }

        private void OnPressed(SelectEnterEventArgs args)
        {
            BallProvider.CreateBalls();
        }
    }
}
