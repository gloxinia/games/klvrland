using UnityEngine;

namespace Klvrland.Struckout
{
    public class TargetPanel : MonoBehaviour
    {
        public Struckout Struckout;

        private bool IsDead = false;
        private Rigidbody Rigidbody;

        private readonly float ForceRatio = 0.5f;

        private void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag(Tag.Bullet.ToString()) && IsDead == false)
            {
                Struckout.Hit();
                IsDead = true;

                Vector3 Force = new Vector3(0.0f, 0.0f, 1.0f);
                Rigidbody.AddForce(Force * ForceRatio, ForceMode.Impulse);
            }
        }
    }
}
