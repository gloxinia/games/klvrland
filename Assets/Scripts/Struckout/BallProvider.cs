using UnityEngine;

namespace Klvrland.Struckout
{
    public class BallProvider : MonoBehaviour
    {
        public GameObject BallPrefab;
        public GameObject BallProvidePoint;

        public void CreateBalls()
        {
            Instantiate(BallPrefab, BallProvidePoint.transform.position, BallProvidePoint.transform.rotation);
        }
    }
}
