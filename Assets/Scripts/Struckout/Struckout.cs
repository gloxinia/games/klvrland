using UnityEngine;

namespace Klvrland.Struckout
{
    public class Struckout : MonoBehaviour
    {
        public StruckoutDirector Director;

        public void Hit()
        {
            Director.Hit();
        }
    }
}
