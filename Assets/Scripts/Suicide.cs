using UnityEngine;

public class Suicide : MonoBehaviour
{
    public float LifeTime = 5.0f;
    private float LifeTimer = 0.0f;

    void Update()
    {
        LifeTimer += Time.deltaTime;

        if (LifeTime < LifeTimer)
        {
            Destroy(this.gameObject);
        }
    }
}
