using UnityEngine;

namespace Klvrland.Shooting
{
    public class TargetHead : MonoBehaviour
    {
        public GameObject Target;

        private void OnTriggerEnter(Collider other)
        {
            Target.GetComponent<Target>().HitToHead();
            Target.GetComponent<TargetRotate>().Hit();
        }
    }
}
