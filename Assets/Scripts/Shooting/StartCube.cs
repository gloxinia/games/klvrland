using UnityEngine;

namespace Klvrland.Shooting
{
    public class StartCube : MonoBehaviour
    {
        public ShootingDirector Director;

        private void OnTriggerEnter(Collider other)
        {
            Director.StartGame();
        }
    }
}
