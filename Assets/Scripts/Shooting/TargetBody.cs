using UnityEngine;

namespace Klvrland.Shooting
{
    public class TargetBody : MonoBehaviour
    {
        public GameObject Target;

        private void OnTriggerEnter(Collider other)
        {
            Target.GetComponent<Target>().HitToBody();
            Target.GetComponent<TargetRotate>().Hit();
        }
    }
}
