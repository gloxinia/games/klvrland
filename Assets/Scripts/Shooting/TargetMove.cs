using UnityEngine;

namespace Klvrland.Shooting
{
    public class TargetMove : MonoBehaviour
    {
        public bool IsMovable = false;
        [Range(1.0f, 10.0f)]
        public float Cycle = 1.0f;
        [Range(0.0f, 1.0f)]
        public float InitialPosition = 0.0f;
        [Range(0.0f, 5.0f)]
        public float MaxPosition = 5.0f;

        private Vector3 SelfPositon;

        private void Awake()
        {
            SelfPositon = transform.position;
        }

        private void Update()
        {
            if (IsMovable)
            {
                Vector3 Position = transform.position;
                Position.x = GetPositionX();
                transform.position = Position;
            }
        }

        private float GetPositionX()
        {
            float Sin = Mathf.Sin(Mathf.PI * 2 * (Time.time + InitialPosition) * (1 / Cycle));
            return SelfPositon.x + Sin * MaxPosition;
        }
    }
}
