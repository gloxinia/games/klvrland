using UnityEngine;
using UnityEngine.UI;

namespace Klvrland.Shooting
{
    public class ShootingDirector : MonoBehaviour
    {
        public Text TextTimer;
        public Text TextScore;
        public Text TextHighScore;
        public AudioSource Speaker;
        public AudioClip SoundStart;
        public AudioClip SoundFinish;
        public AudioClip SoundHitPlus;
        public AudioClip SoundHitMinus;

        private bool IsInGame = false;
        private float Timer = 0.0f;
        private int Score = 0;
        private int HighScore = 0;

        private readonly float TimeLimit = 30.0f;

        private void Awake()
        {
            Timer = TimeLimit;
        }

        private void Update()
        {
            if (IsInGame)
            {
                Timer -= Time.deltaTime;

                if (Timer < 0)
                {
                    FinishGame();
                }
            }

            UpdateText();
        }

        private void UpdateText()
        {
            TextTimer.text = $"Timer: {Timer:F2}";
            TextScore.text = $"Score: {Score}";
            TextHighScore.text = $"High Score: {HighScore}";
        }

        public void StartGame()
        {
            IsInGame = true;
            Timer = TimeLimit;
            Score = 0;
            Speaker.PlayOneShot(SoundStart);
        }

        public void FinishGame()
        {
            IsInGame = false;
            Timer = TimeLimit;

            if (Score > HighScore)
            {
                HighScore = Score;
            }

            Speaker.PlayOneShot(SoundFinish);
        }

        public void AddScore(int score)
        {
            if (IsInGame)
            {
                Score += score;
            }

            if (score > 0)
            {
                Speaker.PlayOneShot(SoundHitPlus);
            }
            else
            {
                Speaker.PlayOneShot(SoundHitMinus);
            }
        }
    }
}
