using UnityEngine;

namespace Klvrland.Shooting
{
    public class TargetRotate : MonoBehaviour
    {
        private bool IsDead = false;
        private float RevivalTimer = 0.0f;
        private float RotationX = 0.0f;

        private readonly float RevivalTime = 1.0f;
        private readonly float DefaultRotation = 0.0f;
        private readonly float FinalRotation = 75.0f;
        private readonly float DownRotateSpeed = 360.0f;
        private readonly float UpRotateSpeed = 45.0f;

        private void Update()
        {
            UpdateRotationX();

            Vector3 angle = transform.localEulerAngles;
            angle.x = RotationX;
            transform.localEulerAngles = angle;
        }

        private void UpdateRotationX()
        {
            if (IsDead)
            {
                // 死んでいる場合
                if (RotationX < FinalRotation)
                {
                    // 最後まで倒れていない場合 倒れる
                    RotationX += DownRotateSpeed * Time.deltaTime;
                }
                else
                {
                    // 最後まで倒れた場合 復活時間を計測する
                    RevivalTimer += Time.deltaTime;
                }

                if (RevivalTime < RevivalTimer)
                {
                    // 復活時間を過ぎた場合 生き返る
                    // このブロックを抜け次フレームから通過しない
                    IsDead = false;
                }
            }
            else
            {
                // 生きている場合
                if (RotationX > DefaultRotation)
                {
                    // 少しでも倒れている場合 起き上がる
                    RotationX -= UpRotateSpeed * Time.deltaTime;
                }
            }
        }

        public void Hit()
        {
            if (IsDead == false)
            {
                IsDead = true;
                RevivalTimer = 0.0f;
            }
        }
    }
}
