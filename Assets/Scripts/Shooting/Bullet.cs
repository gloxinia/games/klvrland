using UnityEngine;

namespace Klvrland.Shooting
{
    public class Bullet : MonoBehaviour
    {
        public GameObject HitEffectPrefab;

        /// <summary>
        /// Moving speed (m/s)
        /// </summary>
        public float Speed { get; set; } = 0.0f;

        private float LifeTimer = 0.0f;

        private readonly float LifeTime = 2.0f;

        private void Update()
        {
            LifeTimer += Time.deltaTime;
            transform.Translate(Vector3.forward * Time.deltaTime * Speed);

            if (LifeTime < LifeTimer)
            {
                Destroy(this.gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tag.Enemy.ToString()))
            {
                Instantiate(HitEffectPrefab, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }
}
