using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Klvrland.Shooting
{
    public class Handgun : MonoBehaviour
    {
        public GameObject Muzzle;
        public GameObject BulletPrefab;
        public GameObject HitEffectPrefab;
        public GameObject LefthandController;
        public GameObject RighthandController;
        public AudioSource Speaker;
        public AudioClip SoundShoot;

        private bool IsCool = false;
        private float CoolTimer = 0.0f;
        private XRBaseInteractable Interactable;
        private Animator Animator;
        private XRInteractorLineVisual LeftLine;
        private XRInteractorLineVisual RightLine;

        private readonly float CoolTime = 0.25f;
        private readonly float BulletSpeed = 50.0f;

        private void Awake()
        {
            Interactable = GetComponent<XRBaseInteractable>();
            Animator = GetComponent<Animator>();
            LeftLine = LefthandController.GetComponent<XRInteractorLineVisual>();
            RightLine = RighthandController.GetComponent<XRInteractorLineVisual>();
        }

        private void OnEnable()
        {
            Interactable.selectEntered.AddListener(SelectEntered);
            Interactable.selectExited.AddListener(SelectExited);
            Interactable.activated.AddListener(Activated);
        }

        private void OnDisable()
        {
            Interactable.selectEntered.RemoveListener(SelectEntered);
            Interactable.selectExited.RemoveListener(SelectExited);
            Interactable.activated.RemoveListener(Activated);
        }

        private void Update()
        {
            CoolTimer += Time.deltaTime;

            if (IsCool == false && CoolTime < CoolTimer)
            {
                IsCool = true;
            }
        }

        private void SelectEntered(BaseInteractionEventArgs args)
        {
            LeftLine.enabled = false;
            RightLine.enabled = false;
        }

        private void SelectExited(BaseInteractionEventArgs args)
        {
            LeftLine.enabled = true;
            RightLine.enabled = true;
        }

        private void Activated(BaseInteractionEventArgs args)
        {
            if (IsCool)
            {
                GameObject ShootedBullet = Instantiate(BulletPrefab, transform.position, transform.rotation);
                ShootedBullet.GetComponent<Bullet>().Speed = BulletSpeed;

                IsCool = false;
                CoolTimer = 0.0f;

                Animator.SetTrigger("Shoot");
                //Instantiate(HitEffectPrefab, Muzzle.transform);
                Speaker.PlayOneShot(SoundShoot);
            }
        }
    }
}
