using UnityEngine;

namespace Klvrland.Shooting
{
    public class Target : MonoBehaviour
    {
        public ShootingDirector Dicretor;
        public int Score;

        public void HitToHead()
        {
            Dicretor.AddScore(Score * 2);
        }

        public void HitToBody()
        {
            Dicretor.AddScore(Score);
        }
    }
}
