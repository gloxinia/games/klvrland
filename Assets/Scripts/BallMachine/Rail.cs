using System;

namespace Klvrland.BallMachine
{
    public struct Coords
    {
        public Coords(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float X { get; set; }
        public float Y { get; set; }
    }

    public class Rail
    {
        public float Progress { get; private set; }
        public Coords Coords { get; private set; }

        private readonly float InitialProgress;
        private readonly float Speed;
        private readonly float Radius;
        private readonly float Height;
        private readonly float Length;

        public Rail(float initialProgress, float speed, float radius, float height)
        {
            InitialProgress = initialProgress;
            Speed = speed;
            Radius = radius;
            Height = height;
            Length = GetLength();

            Progress = InitialProgress;
            Coords = GetCoords();
        }

        public void Move()
        {
            Progress = (Progress + Speed) % 1.0f;
            Coords = GetCoords();
        }

        private float GetLength()
        {
            return 2.0f * (float)Math.PI * Radius + 2.0f * Height;
        }

        private Coords GetCoords()
        {
            float Distance = Progress * Length;
            float HalfCircle = (float)Math.PI * Radius;
            float X;
            float Y;

            if (0 <= Distance && Distance < 0.5f * Height)
            {
                X = Radius;
                Y = Distance;
            }
            else if (0.5f * Height <= Distance && Distance < 0.5f * Height + HalfCircle)
            {
                float CircleProgress = (Distance - 0.5f * Height) / HalfCircle; // 0 ~ 1
                float Radian = CircleProgress * (float)Math.PI;
                X = (float)Math.Cos(Radian) * Radius;
                Y = (float)Math.Sin(Radian) * Radius + 0.5f * Height;
            }
            else if (0.5f * Height + HalfCircle <= Distance && Distance < 1.5f * Height + HalfCircle)
            {
                X = -Radius;
                Y = Height + HalfCircle - Distance;
            }
            else if (1.5f * Height + HalfCircle <= Distance && Distance <= Length - 0.5f * Height)
            {
                float CircleProgress = 1 - (Distance - Height * 1.5f - HalfCircle) / HalfCircle; // 1 ~ 0
                float Radian = CircleProgress * (float)Math.PI;
                X = (float)Math.Cos(Radian) * Radius;
                Y = -((float)Math.Sin(Radian) * Radius + 0.5f * Height);
            }
            else
            {
                X = Radius;
                Y = Distance - Length;
            }

            return new Coords(X, Y);
        }
    }
}
