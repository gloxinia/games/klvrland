using UnityEngine;

namespace Klvrland.BallMachine
{
    public class RailController : MonoBehaviour
    {
        public float InitialProgress = 0.0f;
        public float Speed = 0.0f;
        public float Radius = 1.0f;
        public float Height = 2.0f;

        private Rail Rail;
        private Vector3 InitialPosition;

        private void Awake()
        {
            Rail = new Rail(InitialProgress, Speed, Radius, Height);
            InitialPosition = transform.localPosition;
        }

        private void Update()
        {
            Rail.Move();
            transform.localPosition = InitialPosition + new Vector3(Rail.Coords.X, Rail.Coords.Y, 0.0f);
        }
    }
}
